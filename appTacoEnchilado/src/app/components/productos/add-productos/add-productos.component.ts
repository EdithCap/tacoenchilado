import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Producto } from 'src/app/models/producto.model';
import { ProductoService } from 'src/app/services/producto.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-add-productos',
	templateUrl: './add-productos.component.html',
})
export class AddProductosComponent implements OnInit {

	productoForm: FormGroup;

	constructor(public productoService: ProductoService) { }


	ngOnInit() {
		this.productoForm = new FormGroup({
			'nombre': new FormControl(''),
		});

	}

	save() {
		console.log(this.productoForm.value.nombre)
		let producto = new Producto();
		producto.nombre = this.productoForm.value.nombre;
		this.productoService.saveProducto(producto).subscribe(res => {
			console.log(res);
		});
	}

	

}
