import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Producto } from '../models/producto.model';

@Injectable({
	providedIn: 'root'
})
export class ProductoService {

	private apiUrl = 'http://localhost:27017/api/';
	constructor(private http: HttpClient) { }

	getProductos() {
		const url = this.apiUrl + 'productos';
		return this.http.get<Array<Producto>>(url);
	}

	getProducto(productoId: string) {
		const url = this.apiUrl + 'producto/' + encodeURI(productoId.toString());
		return this.http.get<Producto>(url);
	}


	saveProducto(producto: Producto) {
		const url = this.apiUrl + 'producto';
		return this.http.post<Producto>(url, { producto });
	}
}
