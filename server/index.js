'use strict'

var app = require('./app');
var port = process.env.PORT || 27017;

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/db_taco_enchilado', { useNewUrlParser: true, useUnifiedTopology: true }, (err, res) => {
    if (err) {
        throw err;
    } else {
        console.log('Conexion establecida con Mongodb...');
        app.listen(port, function () {
            console.log('Servidor de api rest escuchando en http://localhost:' + port);
        })
    }
});
