'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PrecioSchema = new Schema({
    valor: Number,
    fechaInicio: Date,
    fechaFin: Date,
    idProducto: {type: Schema.ObjectId, ref: 'producto'}
})

module.exports = mongoose.model('precio', PrecioSchema);
