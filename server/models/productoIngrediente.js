'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ProductoIngredienteSchema = new Schema({
    cantidad: Number,
    idProducto: {type: Schema.ObjectId, ref: 'producto'},
    idIngrediente: {type: Schema.ObjectId, ref: 'ingrediente'}
})

module.exports = mongoose.model('producto_ingrediente', ProductoIngredienteSchema);