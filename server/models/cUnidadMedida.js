'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var CUnidadMedidaSchema = new Schema({
    nombre: String,
})

module.exports = mongoose.model('c_unidad_medida', CUnidadMedidaSchema);