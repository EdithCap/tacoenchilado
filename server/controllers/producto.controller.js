'use strict'

var Producto = require('../models/producto');
var Precio = require('../models/precio')

function saveProducto(req, res) {
    var producto = new Producto();
    var params = req.body;

    producto.nombre = params.nombre;

    producto.save((err, productoStored) => {
        if (err)
            return res.status(500).send({ message: 'Error al guardar la tabla' });

        var precio = new Precio();
        precio.valor = params.valor;
        precio.fechainicio = params.fechainicio;
        precio.fechaFin = params.fechaFin;
        precio.productoId = productoStored.id;

        precio.save((err, precioStored) => {
            if (err)
                return res.status(500).send({ message: 'Error al guardar la tabla' });

            return res.status(200).send({ producto: productoStored });
        });
    });
}

function updateProducto(req, res) {
    let params = req.body;
    let productoId = params.productoId;
    Producto.findById(productoId, (err, producto) => {
        if (err)
            return res.status(500).send({ message: 'Error en la petición' });

        if (!producto)
            return res.status(400).send({ message: 'Error al obtener el registro' });

        producto.nombre = params.nombre;
        Producto.update(producto, (err, productoUpdated) => {
            if (err)
                return res.status(500).send({ message: 'Error en la petición' });

            if (!productoUpdated)
                return res.status(400).send({ message: 'Error al actualizar el registro' });

            return res.status(200).send({ productoUpdated: productoUpdated });
        });
    });
}

function getProduto(req, res) {
    let params = req.body;
    let productoId = params.productoId;
    Producto.findById(productoId, (err, producto) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición' });
        } else {
            if (!producto)
                return res.status(400).send({ message: 'Error al obtener el registro' });

            return res.status(200).send({ producto: producto });
        }
    });
}

function getProdutos(req, res) {
    Producto.find((err, productos) => {
        if (err)
            return res.status(500).send({ message: 'Error en la petición' });

        if (!productos)
            return res.status(400).send({ message: 'Error al obtener los registros' });

        return res.status(200).send({ productos: productos });

    });
}

module.exports = {
    saveProducto,
    getProduto,
    getProdutos,
    updateProducto
}