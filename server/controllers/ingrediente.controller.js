'use strict'

var Ingrediente = require('../models/ingrediente');

function saveIngrediente(req, res) {
    var ingrediente = new Ingrediente();
    var params = req.body;

    ingrediente.nombre = params.nombre;
    ingrediente.bRefrigerado = params.bRefrigerado;
    ingrediente.idUnidad = params.idUnidad;

    ingrediente.save((err, ingredienteStored) => {
        if (err)
            return res.status(500).send({ message: 'Error al guardar la tabla' });

        return res.status(200).send({ ingrediente: ingredienteStored });
    });
}

function getIngredientes(req, res) {
    Ingrediente.find((err, ingredientes) => {
        if (err)
            return res.status(500).send({ message: 'Error en la petición' });

        if (!ingredientes)
            return res.status(400).send({ message: 'Error al obtener el registro' });

        return res.status(200).send({ ingredientes: ingredientes });
    });
}

module.exports = {
    saveIngrediente,
    getIngredientes
}