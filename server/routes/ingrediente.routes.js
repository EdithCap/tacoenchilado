'use strict'

var express = require('express');
var IngredienteController = require('../controllers/ingrediente.controller');

var api = express.Router();

api.get('/ingredientes', IngredienteController.getIngredientes);

api.post('/ingrediente', IngredienteController.saveIngrediente);

module.exports = api;